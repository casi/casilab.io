import * as React from 'react'

import "./404.scss"

const NotFoundPage = () => (
  <div>
    <h1 className="number404">404</h1>
    <h3 className="sub404">esse site está de greve</h3>
    <h4 className="sub404">volte próximo semestre</h4>
    <h4 className="sub404">{"(╯°□°）╯︵ ┻━┻"}</h4>
  </div>
)

export default NotFoundPage
