import * as React from "react";
import "./contact.scss";
import * as emailjs from "emailjs-com";

export type StatusSend = "notSended" | "sending" | "sended" | "errorOnSend";

interface IState {
  name: string;
  email: string;
  description: string;
  sendStatus: StatusSend;
}

const tokens = {
  templateID: "template_rFGPLDF9",
  serviceID: "casicontatozoho",
  userID: "user_Zpc7qMDBZEucIz16SIujj"
};

export default class ContactPage extends React.Component<{}, IState> {
  state = {
    name: "",
    email: "",
    description: "",
    sendStatus: "notSended" as StatusSend
  };

  private cleanFormState = () => {
    this.setState({
      ...this.state,
      name: "",
      email: "",
      description: ""
    });
  };

  private setSendStatus = (status: StatusSend) => {
    this.setState({
      ...this.state,
      sendStatus: status
    });
  };

  private submitForm = async (ev: React.FormEvent<any>) => {
    ev.preventDefault();
    try {
      this.setSendStatus("sending");
      const promiseReturn = await emailjs.send(
        tokens.serviceID,
        tokens.templateID,
        {
          name: this.state.name,
          email: this.state.email,
          description: this.state.description
        },
        tokens.userID
      );
      if (
        promiseReturn &&
        promiseReturn.status === 200 &&
        promiseReturn.text === "OK"
      ) {
        this.setSendStatus("sended");
      } else {
        this.setSendStatus("errorOnSend");
      }
      console.log(promiseReturn);
    } catch (err) {
      this.setSendStatus("errorOnSend");
      // console.log("ERRRo", err);
    }
    this.cleanFormState();
  };

  public render() {
    return (
      <div>
        <h1>Contato</h1>
        <form onSubmit={this.submitForm}>
          <div className="form-field">
            <div className="form-label-wrapper">nome:</div>
            <div className="form-input-wrapper">
              <input
                value={this.state.name}
                onChange={ev =>
                  this.setState({ ...this.state, name: ev.target.value })
                }
                className="form-input"
              />
            </div>
          </div>
          <div className="form-field">
            <div className="form-label-wrapper">email:</div>
            <div className="form-input-wrapper">
              <input
                value={this.state.email}
                onChange={ev =>
                  this.setState({ ...this.state, email: ev.target.value })
                }
                className="form-input"
              />
            </div>
          </div>
          <div className="form-field">
            <div className="form-label-wrapper">mensagem:</div>
            <div className="form-input-wrapper">
              <textarea
                value={this.state.description}
                onChange={ev =>
                  this.setState({ ...this.state, description: ev.target.value })
                }
                rows={4}
                className="form-input"
              />
            </div>
          </div>
          {this.state.sendStatus === "sended" || this.state.sendStatus === "errorOnSend"? 
            <div className={"formalert "+ (this.state.sendStatus === "sended"? "success" : "error")}>
              {this.state.sendStatus === "sended"? "Mensagem enviada" : "Erro ao enviar mensagem"}
            </div>
          :null}
          <div className="form-field">
            <div className="form-label-wrapper" />
            <div className="form-input-wrapper">
              <button
                className="form-button"
                disabled={this.state.sendStatus === "sending"}
              >
                {this.state.sendStatus === "sending" ? "Enviando..." : "Enviar"}
              </button>
            </div>
          </div>
        </form>
      </div>
    );
  }
}
