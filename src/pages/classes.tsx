import * as React from 'react'
import Link from 'gatsby-link'

import "./classes.scss"




interface classType {
  name: string;
  code: string;
  googleGroups?: string;
  colorClassName?: "verde" | "amarelo" | "azul" | "rosa" | "laranja" | "cinza" ; 
  // phone?: string;
  // email?: string;
  // urlLates?: string;
}

interface semesterType{
  semesterNumber: number;
  classes: classType[];
}

const semesters : semesterType[] = [
  {
    semesterNumber: 1,
    classes:[
      {name: "Sociologia",colorClassName: "verde",code: "CIS465", googleGroups: "sociologia-CIS465"},
      {name: "Comunicação e Expressão",colorClassName: "laranja", code: "LET006", googleGroups: "comunicacao_e_expressao-LET006"},
      {name: "Inglês Instrumental", colorClassName: "laranja",code: "LET127", googleGroups: "ingles_instrumental-LET127"},
      {name: "Lógica e Matemática Discreta", colorClassName: "amarelo",code: "MAT002", googleGroups: "logica_e_matematica_discreta-MAT002"},
      {name: "Algoritmos", colorClassName: "amarelo",code: "CPD063", googleGroups: "algoritmos-CPD063"},
      {name: "Fundamentos de Informática", colorClassName: "amarelo",code: "CPD004", googleGroups: "fundamentos_de_informatica-CPD004"},
    ]
  },
  {
    semesterNumber: 2,
    classes:[
      {name: "Filosofia da Ciência", colorClassName: "verde",code: "CIS052",googleGroups: "filosofia_da_ciencia-CIS052"},
      {name: "Sistemas de Informação", colorClassName: "amarelo",code: "CPD005", googleGroups:"sistemas_de_informacao-CPD005"},
      {name: "Cálculo I", colorClassName: "amarelo",code: "MAT169", googleGroups:"calculo_1-MAT169"},
      {name: "Estrutura de Dados I", colorClassName: "amarelo",code: "CPD006", googleGroups: "estrutura_de_dados_1-CPD006"},
      {name: "Linguagem de Programação I", colorClassName: "amarelo",code: "CPD007",googleGroups:"linguagem_de_programacao_1-CPD007"},
      {name: "Arquitetura de Computadores", colorClassName: "amarelo",code: "CPD008",googleGroups:"arquitetura_de_computadores-CPD008"},
    ]
  },
  {
    semesterNumber: 3,
    classes:[
      {name: "Metodologia da Pesquisa em Informática", colorClassName: "laranja",code: "CPD014", googleGroups:"metodologia_da_pesquisa_em_informatica-CPD014"},
      {name: "Teoria Geral da Administração", colorClassName: "rosa",code: "ADM099", googleGroups:"teoria_geral_da_administracao-ADM099"},
      {name: "Cálculo II", colorClassName: "amarelo",code: "MAT005", googleGroups:"calculo_2-MAT005"},
      {name: "Estrutura de Dados II", colorClassName: "amarelo",code: "CPD015", googleGroups:"estrutura_de_dados_2-CPD015"},
      {name: "Linguagem de Programação II", colorClassName: "amarelo",code: "CPD019", googleGroups:"linguagem_de_programacao_2-CPD019"},
      {name: "Sistemas Operacionais", colorClassName: "azul",code: "CPD020", googleGroups:"sistemas_operacionais-CPD020"},
    ]
  },
  {
    semesterNumber: 4,
    classes:[
      {name: "Probabilidade e Estatística", colorClassName: "amarelo",code: "MAT066", googleGroups:"probabilidade_e_estatistica-MAT066"},
      {name: "Contabilidade", colorClassName: "rosa",code: "CON080", googleGroups:"contabilidade-CON080"},
      {name: "Metodologia de Desenvolvimento de Sistemas I", colorClassName: "azul",code: "CPD021", googleGroups:"metodologia_de_desenvolvimento_de_sistemas_1-CPD021"},
      {name: "Banco de Dados I", colorClassName: "azul",code: "CPD022", googleGroups:"banco_de_dados_1-CPD022"},
      {name: "Linguagem de Programação III", colorClassName: "amarelo",code: "CPD023", googleGroups:"linguagem_de_programacao_3-CPD023"},
      {name: "Redes de Computadores I", colorClassName: "azul",code: "CPD024", googleGroups:"redes_de_computadores_1-CPD024"},
    ]
  },
  {
    semesterNumber: 5,
    classes:[
      /* {name: "Optativa", colorClassName: "verde",code: ""},*/
      {name: "Economia", colorClassName: "rosa",code: "ECO262"},
      {name: "Metodologia de Desenvolvimento de Sistemas II", colorClassName: "azul",code: "CPD025"},
      {name: "Banco de Dados II", colorClassName: "azul",code: "CPD026"},
      {name: "Fundamentos de Compiladores", colorClassName: "azul",code: "CPD027"},
      {name: "Redes de Computadores II", colorClassName: "azul",code: "CPD028"},
    ]
  },
  {
    semesterNumber: 6,
    classes:[
      /* {name: "Optativa", colorClassName: "verde",code: ""},*/
      {name: "Psicologia Aplicada às Organizações", colorClassName: "rosa",code: "ADM020"},
      {name: "Projeto Avançado de Sistemas", colorClassName: "azul",code: "CPD029"},
      {name: "Engenharia de Programas", colorClassName: "azul",code: "CPD030"},
      {name: "Interface Humano-Computador", colorClassName: "azul",code: "CPD031"},
      {name: "Sistemas Distribuídos", colorClassName: "azul",code: "CPD032"},
    ]
  },
  {
    semesterNumber: 7,
    classes:[
      /* {name: "Optativa", colorClassName: "verde",code: ""},*/
      {name: "Ética Profissional", colorClassName: "verde",code: "CIS062",googleGroups: "etica_profissional-CIS062"},
      {name: "Sistemas Multimídia", colorClassName: "azul",code: "CPD033",googleGroups: "sistemas_multimidia-CPD033"},
      {name: "Engenharia de Software", colorClassName: "azul",code: "CPD035",googleGroups: "engenharia_de_software-CPD035"},
      {name: "Gerência de Projetos de Sistemas", colorClassName: "azul",code: "CPD040",googleGroups: "gerencia_de_projetos_de_sistemas-CPD040"},
      {name: "Estágio Supervisionado", colorClassName: "laranja",code: "CPD041",googleGroups: "estagio_supervisionado-CPD041"},
    ]
  },
  {
    semesterNumber: 8,
    classes:[
      /* {name: "Optativa", colorClassName: "verde",code: ""},*/
      {name: "Empreendedorismo", colorClassName: "laranja",code: "ADM014"},
      {name: "Inteligência Artificial", colorClassName: "azul",code: "CPD042"},
      {name: "Tópicos Especiais em Engenharia de Software", colorClassName: "azul",code: "CPD043"},
      {name: "Trabalho de Conclusão de Curso I", colorClassName: "laranja",code: "CPD044"},
      {name: "Auditoria de Sistemas", colorClassName: "azul",code: "CPD045"},
    ]
  },
  {
    semesterNumber: 9,
    classes:[
      /* {name: "Optativa", colorClassName: "verde",code: ""},*/
      {name: "Tópicos Especiais em Banco de Dados", colorClassName: "azul",code: "CPD046"},
      {name: "Trabalho de Conclusão de Curso II", colorClassName: "laranja",code: "CPD047"},
      {name: "Computadores e Sociedade", colorClassName: "verde",code: "CPD048"},
    ]
  },
  {
    semesterNumber: 99,
    classes:[
      {name: "Cálculo III", colorClassName: "cinza",code: "MAT007"},
      {name: "Organização, Sistemas e Métodos", colorClassName: "cinza",code: "ADM027"},
      {name: "Teoria dos Grafos", colorClassName: "cinza",code: "CPD059"},
      {name: "Álgebra Linear", colorClassName: "cinza",code: "MAT008"},
      {name: "Administração de Recursos Humanos", colorClassName: "cinza",code: "ADM009"},
      {name: "Física Geral", colorClassName: "cinza",code: "FIS003 "},
      {name: "Engenharia Economica", colorClassName: "cinza",code: "CPD049", googleGroups: "engenharia_de_economica-CPD049"},
      {name: "Pesquisa Operacional", colorClassName: "cinza",code: "CPD050"},
      {name: "Sistemas Digitais", colorClassName: "cinza",code: "CPD051"},
      {name: "Ambiente de Negócios e Marketing", colorClassName: "cinza",code: "ADM022 "},
      {name: "Cálculo Numérico", colorClassName: "cinza",code: "MAT046"},
      {name: "Automação Industrial", colorClassName: "cinza",code: "CPD052"},
      {name: "Logística", colorClassName: "cinza",code: "CPD053"},
      {name: "Tópicos Especiais em Linguagens de Programação", colorClassName: "cinza",code: "CPD054"},
    ]
  },
]




class teacherPage extends React.Component<{}, {}> {
  public render() {
    const noInfo = "Sem informação";

    const rows = semesters.map((sem)=> {
      const semesterLabel = sem.semesterNumber !== 99 ? sem.semesterNumber+ "º Semestre" : "Optativas";

      const semseterRows = sem.classes.map(
          (cla, index) => {
            const nameCell = <td className={cla.colorClassName || "cinza"}>{cla.name}</td>
            const codeCell = <td className={cla.colorClassName || "cinza"}>{cla.code}</td>
            const email = cla.googleGroups? cla.googleGroups.toLowerCase()+"@googlegroups.com" : undefined;
            const googleGroupsCell = (
              <td className={cla.colorClassName || "cinza"}>{cla.googleGroups?
                  <a href={"mailto:"+email}>{email}</a>
                  :"Sem grupo de email ainda"}
              </td>)
            const googleGroupsCellLink = (
              <td className={cla.colorClassName || "cinza"}>{cla.googleGroups?
                <a
                  href={"https://groups.google.com/d/forum/"+cla.googleGroups.toLowerCase()}
                  target="_blank"  rel="noreferrer">
                    Link para o grupo
                </a>
                :"Sem grupo de email ainda"}
              </td>)

            return (
              <tr>
                {index === 0 ? <td rowSpan={sem.classes.length}>{semesterLabel}</td> : null}
                {nameCell}
                {codeCell}
                {googleGroupsCell}
                {googleGroupsCellLink}
              </tr>
            )
        }
      );
      return semseterRows;
    })

    return (
      <div>
        <div className="titleWrapperEmoji">
          <h1 className="titleOnWrapper">Informações das disciplinas</h1>
          <h1 className="titleEmoji">📚</h1>
        </div>
        <h4>Legenda</h4>
        <div className="legenda">
          <div className="box verde">FORMAÇÃO HUMANÍSTICA</div>
          <div className="box rosa">FORMAÇÃO COMPLEMENTAR</div>
          <div className="box azul">FORMAÇÃO TECNOLÓGICA</div>
          <div className="box amarelo">FORMAÇÃO BÁSICA</div>
          <div className="box laranja">FORMAÇÃO SUPLEMENTAR</div>
          <div className="box cinza">DISCIPLINAS OPTATIVAS</div>
        </div>

        {/* <img src="http://www.csi.uneb.br/images/Fluxograma.png" /> */}
        <div className="tablewrapper" >
        <table style={{width:"100%"}}>
          <tr>
            <th width="100px">Semestre</th>
            <th>Disciplina</th>
            <th>Código</th>
            <th colSpan={2}>Grupo de email</th>
          </tr>
          {rows}
        </table>
        </div>
      </div>
    )
  }
}


export default teacherPage
