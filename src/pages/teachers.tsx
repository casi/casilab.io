import * as React from "react";
import Link from "gatsby-link";

import "./teachers.scss";

interface teacher {
  name: string;
  phone?: string;
  email?: string;
  urlLates?: string;
}

const teacherList: teacher[] = [
  {
    name: "Antônio Carlos Fontes Atta",
    email: "antonio.atta@gmail.com",
    urlLates: "http://lattes.cnpq.br/3591777818200356"
  },
  {
    name: "Antônio Marcos Brito Cerqueira",
    email: "marcosbcerqueira@yahoo.com.br",
    urlLates: "http://lattes.cnpq.br/2580696152419927"
  },
  {
    name: "Cláudio Alves de Amorim",
    email: "clamorim67@gmail.com",
    urlLates: "http://lattes.cnpq.br/4976012823452609"
  },
  {
    name: "Débora Alcina Rego Chaves",
    email: "dchaves@uneb.br",
    urlLates: "http://lattes.cnpq.br/4638465164256598"
  },
  {
    name: "Diego Gervásio Frias Suarez",
    email: "diegofriass@gmail.com",
    urlLates: "http://lattes.cnpq.br/1377022547816780"
  },
  {
    name: "Eduardo Manoel de Freitas Jorge",
    email: "emjorge1974@gmail.com",
    urlLates: "http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K4751931E1"
  },
  {
    name: "Jorge Sampaio Farias",
    email: "jfarias@uneb.br",
    urlLates: "http://lattes.cnpq.br/6683499592786376"
  },
  {
    name: "Josemar Sampaio Farias",
    email: "josemarbr@gmail.com",
    urlLates: "http://http//lattes.cnpq.br/5463076295727564"
  },
  {
    name: "Julian Hermógenes Quezeda Céledon",
    email: "julianqueseda@gmail.com",
    urlLates: "http://lattes.cnpq.br/8577194337328753"
  },
  {
    name: "Leandro Santos Coelho de Souza",
    email: "lscs21@gmail.com",
    urlLates: "http://lattes.cnpq.br/1474574195753600"
  },
  {
    name: "Marco Antônio Costa Simões",
    email: "marcossimoes@uneb.br",
    urlLates: "http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K4707035Z2"
  },
  {
    name: "Maria Inês Valderrama Restovic",
    email: "mrestovic@uneb.br",
    urlLates: "http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K4723869H7"
  },
  {
    name: "Maria Lívia Astoufo Coutinho",
    email: "marialivia@hotmail.com",
    urlLates: "http://lattes.cnpq.br/1345987638726151"
  },
  {
    name: "Trícia Santos Couto",
    email: "tricia.ssantos@gmail.com",
    urlLates: "http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K4728748A0"
  },{
    name: "Artur Henrique Kronbauer",
    email: "arthurhk@gmail.com",
  },{
    name: "Hugo Saba Pereira Cardoso",
  }
];

class teacherPage extends React.Component<{}, {}> {
  public render() {
    const teachers = teacherList.map(t => {
      const noInfo = "Sem informação";
      const linkLates = (
        <a target="_blank" href={t.urlLates}  rel="noreferrer">
          Link
        </a>
      );
      return (
        <tr>
          <td>{t.name ? t.name : noInfo}</td>
          <td>{t.phone ? t.phone : noInfo}</td>
          <td>{t.email ? t.email : noInfo}</td>
          <td style={{textAlign:"center"}}>{t.urlLates ? linkLates : noInfo}</td>
        </tr>
      );
    });

    return (
      <div>
        <div className="titleWrapperEmoji">
          <h1 className="titleOnWrapper">Informações dos professores</h1>
          <h1 className="titleEmoji">💁</h1>
        </div>

        <div className="tablewrapper">
          <table style={{ width: "100%" }}>
            <tr>
              <th>nome</th>
              <th>telefone</th>
              <th>email</th>
              <th>lattes</th>
            </tr>
            {teachers}
          </table>
        </div>

        {/* <p>Welcome to page 2</p> */}
        {/* <Link to="/">Go back to the homepage</Link> */}
      </div>
    );
  }
}

export default teacherPage;
