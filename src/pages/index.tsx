import * as React from 'react'

import "./index.scss";

interface IndexPageProps {
  data:{
      site:{
          siteMetadata:{
              title: string;
          }
      }
  }
}

export default class Home extends React.Component<IndexPageProps, {}> {
  constructor(props: IndexPageProps, context: any) {
    super(props, context)
  }
  public render() {
    return (
      <div>
        <div className="titleWrapperEmoji">
            <h1 className="titleOnWrapper">Olá!</h1>
            <h1 className="titleEmoji">👋</h1>
        </div>
        <h2>Seja bem vindo.</h2>
        <p>Esse site está em construção, mas fica tranquilo que já tem coisa no ar. basta olhar ali em cima no menu 👆, beleza??</p>
        <p>Qualquer coisa <strike>basta entrar na pagina de contato</strike>.(HAHAHA ainda não... ela tambem está em construção 😨 )</p>
        <p>Então aproveita ai o que tem 👍!</p>
      </div>
    )
  }
}

export const pageQuery = graphql`
  query IndexQuery {
      site{
          siteMetadata{
              title
          }
      }
  }
`