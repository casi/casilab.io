import * as React from 'react'
import Link from 'gatsby-link'
import "./index.scss"

interface State {
    menuOpen: boolean;
}

interface IMenuItem {
    key: string;
    to: string;
    title: string | JSX.Element;
    external?: boolean;
    target?: string;
    rel?: string;
}

class Header extends React.Component<{}, State> {
    state = {menuOpen: false};


    private flipState = () =>{
        this.setState({menuOpen: !this.state.menuOpen})
    }

    render(){

        const menuItens: IMenuItem[] = [
            {key:"info" ,to: "/info",title:"informações"},
            {key:"teachers" ,to: "/teachers",title:"Professores"},
            {key:"classes" ,to: "/classes",title:"Disciplinas"},
            {key: "slack", to : "https://join.slack.com/t/uneb-si/shared_invite/enQtMzM5NDU4Nzc2MDIxLTgzZTc0ZDliMTQwN2I2NzhmOGI3OWJmZGVlMmFmMTdiNTJjNGI5Y2YzZTQxZmFmM2NkNzY2MzVmMzI0MDZiNDc", title: "Slack", target: "_blank", rel:"noreferrer", external: true},
            {key:"contato" ,to: "/contact",title:"Contato"},
        ]
        const menuItensJSX = menuItens.map(item =>{
            const {title,to,external,...rest} = item;
            return !!external? <a href={to} {...rest}>{title}</a> : <Link {...rest} to={to} >{title}</Link>;
        })

        return (
            <div className="header">
                <div className="title-wrapper">
                    <Link to="/">casi.gitlab.io</Link>
                </div>
                <div className="nav-wrapper-desktop">
                    {menuItensJSX}
                </div>
                <div className="nav-wrapper-mobile">
                    <div className="nav-wrapper-mobile-menu" onClick={this.flipState}>Menu</div>
                    <div className={this.state.menuOpen? "nav-wrapper-mobile-itens-open": "nav-wrapper-mobile-itens-close"}  >{menuItensJSX}</div>
                </div>
                
        </div>
        )
    }
}


export default Header