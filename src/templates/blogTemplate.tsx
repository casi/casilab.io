import * as React from "react";
import Link from 'gatsby-link'



import "./index.scss"


interface dataProp {
  data:{
    markdownRemark:{
      frontmatter:{
        date: Date;
        path: string;
        title: string;
        tags?: string[];
      };
      html: string;
    }
  }
}

export default function Template(
  data: dataProp, // this prop will be injected by the GraphQL query below.
) {
  const { markdownRemark } = data.data; // data.markdownRemark holds our post data
  const { frontmatter, html } = markdownRemark;
  return (
    <div className="blog-post-container">
      <div className="blog-post">
        <div className="go-back-wrapper">
          <Link to="/info">{"Voltar"}</Link>
          <div className="title">{frontmatter.title}</div>
        </div>
        <div className="subtitle">
          <h5 >{frontmatter.date}</h5>
        </div>
        <div
          className="blog-post-content"
          dangerouslySetInnerHTML={{ __html: html }}
        />
      </div>
    </div>
  );
}

export const pageQuery = graphql`
  query BlogPostByPath($path: String!) {
    markdownRemark(frontmatter: { path: { eq: $path } }) {
      html
      frontmatter {
        date(formatString: "MMMM DD, YYYY")
        path
        title
        tags
      }
    }
  }
`;