module.exports = {
  siteMetadata: {
    title: `Casi`,
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    // `gatsby-plugin-webpack-bundle-analyzer`,
    // Add typescript stack into webpack
    `gatsby-plugin-typescript`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/src/md`,
        name: "markdown-pages",
      },
    },
    `gatsby-transformer-remark`,
    `gatsby-plugin-sass`,
    // `gatsby-plugin-offline`,
    {
      resolve: `gatsby-plugin-favicon`,
      options: {
        logo: "./src/favicon.png",
  
        // WebApp Manifest Configuration
        appName: "Casi", // Inferred with your package.json
        appDescription: "Centro academico do curso de sistemas de informação",
        developerName: "kevin oliveira",
        developerURL: "kevinoliveira.gitlab.io",
        dir: 'auto',
        lang: 'pt-BR',
        background: '#02206A',
        theme_color: '#02206A',
        display: 'standalone',
        orientation: 'any',
        start_url: '/',
        version: '1.0',
  
        icons: {
          android: true,
          appleIcon: true,
          appleStartup: true,
          coast: false,
          favicons: true,
          firefox: true,
          opengraph: false,
          twitter: false,
          yandex: false,
          windows: false
        }
      }
    },
  ],
}
